Python Summit 2022
==================

CLI applications & TDD: Never write scripts again
-------------------------------------------------

- [Example of a typical script](script_example)

Exercise:

1. Run the workflow script.
1. Try installing the `helper` package from PyPI, and run the script again.
1. Launch the Python interpreter in the workflow script folder, and import the script.
