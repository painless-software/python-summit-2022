#!/opt/example/backend/python/bin/python3 -u

import os
import random
import sys

# extend path for Python imports
sys.path.insert(0, '/opt/example/backend/custom/bin')

from helper.config import Config
from helper.example import Example
from helper.kubernetes import Operator
from helper.logging import Logger
from helper.mail import Sendmail
from helper.ticketing import Tickets, TicketService


def handle_ticket(ticket_id):

    ticket_service = TicketService()
    ticket = ticket_service.get_ticket(ticket_id)
    if not ticket:
        raise ValueError('No ticket for id %s' % ticket_id)

    task = ticket.get_task()
    details = ticket.get_details()

    if 'pod' in details:
        pod_name = details['pod'].name
        engine = Operator(pod_name, task, ticket)
        device = engine.do_complicated_stuff()
        engine.analyze_result(device, pod_name, details)
        ticket_service.update_task(ticket, task, pod_name)
    else:
        example = Example()
        manager = example.get_manager(str(task), details)
        ticket_service.add_task(ticket, task, manager)


def main():
    setup = Config().read('/opt/example/backend/custom/conf/production.conf')
    logger = Logger().get_logger()

    if len(sys.argv) == 1:
        print("Process tickets in a workflow.")
        print("usage: python workflow.py <ticket_id>")
        sys.exit(0)

    logger.info('Starting script with pid %s', os.getpid())

    try:
        ticket_id = Tickets.parse_ticket_id(sys.argv)
        handle_ticket(ticket_id)
        print("done.")
    except Exception as ex:
        script_name = os.path.splitext(os.path.basename(__file__))[0]
        Sendmail.send_crash_error_notification_mail(script_name, ticket_id, str(ex))
        sys.exit(1)

if __name__ == "__main__":
    main()
