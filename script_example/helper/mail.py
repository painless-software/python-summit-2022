"""
Shared email facilities for scripts.
"""

import smtplib
from email.message import EmailMessage

SENDER = "development@example.com"
RECIPIENT = "support@example.com"


class Sendmail:

    def make_message(sender, recipient, subject, body):
        msg = EmailMessage()
        msg['From'] = sender
        msg['To'] = recipient
        msg['Subject'] = subject
        msg.set_content(body)
        return msg

    def send_crash_error_notification_mail(script_name, ticket_id, description):
        subject = f"Crash report: {script_name} for {ticket_id}"
        message = Sendmail.make_message(SENDER, RECIPIENT, subject, description)
        Sendmail.send_email(message)

    def send_email(message):
        smtp = smtplib.SMTP('localhost')
        smtp.send_message(message)
        smtp.quit()
