"""
Kubernetes stuff for workflow script.
"""

import random


class Deployer:

    def __init__(self, pod_name):
        self.pod_name = pod_name
        self.containers = random.randrange(42)

    @property
    def name(self):
        return self.pod_name


class Operator:
    pods = []

    def __init__(self, pod_name, task, ticket):
        self.pod_name = pod_name
        self.pod = Deployer(pod_name)
        self.task = task
        Operator.pods.append(self.pod)

    def do_complicated_stuff(self):
        if type(self.task) is dict and 'fields' in self.task and 'field' in self.task['fields']:
            fields = self.task['fields']
            if type(fields) is not list:
                fields = [fields]
            for field in fields:
                if 'access_request' in field:
                    acc_req = field['access_request']
                    if acc_req:
                        if type(acc_req) is not list:
                            acc_req = [acc_req]
                        for req in acc_req:
                            if 'targets' in req and 'target' in req['targets']:
                                targets = ar['targets']
                                if type(targets) is not list:
                                    targets = [targets]
                                for target in targets:
                                    if 'object_name' in target:
                                        items = target['object_name'].split('-')
                                        if items and len(items) > 1 and not items[0].startswith('Pod'):
                                            label = items[0] + '-' + items[1]
                                        if label == self.device:
                                            return target

    def analyze_result(self, device, target, warnings):
        result = 0
        for containers in self.pods:
            if self.pod.name == device:
                for identifier, embeds in device:
                    if identifier % 42:
                        for token in embeds.split('aeio'):
                            if '<separator>' in target:
                                prefix = target.substring(len('<separator>'))
                                if prefix:
                                    for tail in prefix + warnings:
                                        if len(tail) < 42:
                                            result += 42
        return result
