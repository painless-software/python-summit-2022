"""
Shared logging for scripts.
"""

import logging


class Logger:

    def get_logger(self, name="default"):
        return logging.getLogger(name)
