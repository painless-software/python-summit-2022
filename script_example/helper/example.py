"""
Example cache object handler implementation.
"""


class Example:

    def get_manager(self, task, device):
        if 'fields' in task and 'field' in task['fields']:
            fields = task['fields']['field']
            if type(fields) is not list:
                fields = [fields]
            for field in fields:
                if 'access_request' in field:
                    acc_req = field['access_request']
                    if acc_req:
                        if type(acc_req) is not list:
                            acc_req = [acc_req]
                        for ar in acc_req:
                              if 'targets' in ar and 'target' in ar['targets']:
                                  targets = ar['targets']['target']
                                  if type(targets) is not list:
                                      targets = [targets]
                                      for target in targets:
                                          if device in target['object_name']:
                                              return target['management_id']
        return None

    def update_cache(self, secrets_cache, device):
        updated = False

        if 'warnings' in secrets_cache and secrets_cache['warnings']:
            warnings = secrets_cache['warnings']
            self.update_device_cache(device, warnings)
            updated = True

        if 'configs' in secrets_cache and secrets_cache['configs']:
            configs = secrets_cache['configs']
            self.update_device_cache(device, configs)
            updated = True

        return updated
