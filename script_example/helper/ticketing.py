"""
Ticket handling shared services for script.
"""

from helper.kubernetes import Deployer


class Task:

    def __init__(self, name):
        self.name = name


class Ticket:

    def __init__(self, number, name, manager, task, details):
        self.number = number
        self.name = name
        self.manager = manager
        self.task = task
        self.details = details

    def get_task(self):
        return self.task

    def get_details(self):
        return self.details


class Tickets:

    def __init__(self):
        self.source = __file__

    @staticmethod
    def parse_ticket_id(args):

        # TODO: smart parsing
        return args[1] if len(args) > 1 else None

    @staticmethod
    def get_task(ticket):

        # TODO: smart selection
        return ticket.get('task')


class TicketService:

    def __init__(self):
        self.manager = __name__

    def get_ticket(self, ticket_id):
        ticket_name = f"Ticket #{ticket_id}"
        details = {
            'description': [
                'some',
                'lines',
                'of',
                'text',
            ],
            'pod': Deployer(ticket_name),
        }
        ticket = Ticket(
            ticket_id,
            ticket_name,
            self.manager,
            Task(f"#{ticket_id}.42"),
            details)
        return ticket

    def add_task(self, ticket, task, manager):
        # TODO: implement properly
        self.update_task(ticket, task, manager)

    def update_task(self, ticket, task, manager):
        ticket.task = task
        ticket.manager = manager
